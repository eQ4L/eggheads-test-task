1. Как вы думаете, что хотел сделать программист? Исправьте код и объясните свое решение
```js
for (var i = 0; i < 5; i++) {
  setTimeout(function () {
    console.log(i);
  }, i * 1000);
}

```

Скорее всего хотелось вывести в консоль сообщения друг за другом с задержкой в секунду между выводами, но выводить не общее количество итераций, а порядковый номер

###### Solution:

Задачка связана с терминами eventLoop, замыкание
setTimeout срабатывает уже после того как цикл закончился. Даже если вы указать setTimeout (func, 0 ). setTimeout все равно выполнится только после цикла, независимо от времени работы цикла
Можно явно указать (забиндить) множитель задержки в функцию
```js
for (var i = 0; i < 5; i++) {
  setTimeout(function (idx) {
    console.log(idx);
  }.bind(null, i), i * 1000);
}

```
Если у можно использовать es6, то вместо var можно использовать let. т.к. на каждый "тик" цикла будет создаваться новая переменная
```js
for (let i = 0; i < 5; i++) {
  setTimeout(function () {
    console.log(i);
  }, i * 1000);
}

```
Также можно явно передать параметры в `setTimeout`, но это не будет работать в старых браузерах < IE9
```js
for (let i = 0; i < 5; i++) {
  setTimeout(function (j) {
    console.log(j);
  }, i * 1000, i);
}
```
Еще одно решение
```js
for (let i = 0; i < 5; i++) {
  (function(j) {
    setTimeout(function () {
      console.log(j);
    }, i * 1000);
  })(i);
}
```

2. Написать функцию, которая будет работать правильно при обоих вызовах, как показано на приведенном коде
```js
console.log(sum(2, 3)); // результат 5
console.log(sum(2)(3)(4)...); // сумма всех элементов
```

###### Solution:

```js
function add(num) {
  const addNext = function(x) {
    return add(num + x);
  };

  addNext.valueOf = function() {
    return num;
  };

  return addNext;
}
```

3. Какая проблема может быть с этим кодом, если список очень длинный? Предложите и объясните свое решение
```js
let list = readHugeList();
let nextListItem = function () {
    let item = list.pop();
    if (item) {
        // ... обработка записи
        nextListItem();
    }
};
```
###### Solution:
Использование рекурсии зачастую выполняется дольше, а также очень легко зациклить функцию.
Рекурсивным обходом можно переполнить стек, тем самым не добиться желаемого результата.
Также мутация исходного массива, на мой взгляд, плохая практика.
Бывают ситуации, когда нужен исходный вид данных.

Если это некий список и нам надо вывести сумму итого или еще что-то, и банальный проход обход данных нам не подходит. Можно попросить бекенд сделать вывод результата, а если это невозможно - в качестве решения можно использовать `веб-воркеры`. Мы можем отрендерить часть (начальные данные) списка (если это список) с помощью `virtual scroll`, а поле итого отобразить в виде скелетона. Когда веб-воркер закончит свою работу - отобразить данные.
А т.к. список большой virtual scroll может помочь с оптимизацией отображения на страничке (приблизиться к 60 фпс отрисовке).

4. Чему будет равна переменная "a" после выполнения этого участка кода? Объясните почему.
```js
let a = 1;
function foo() {
    a = 2;
    return 10;
}
a += foo();
a += foo();
```
###### Solution:
Переменная а после выполнения будет равна `21`.
Оператор `+=` добавляет значение правого операнда к переменной и присваивает переменной результат.
Интерпретатор проходит сверху вниз, выделяется область памяти под переменную `а`, далее под функцию `foo`, далее к переменной а прибавляется результат т.е. действие аналогично записи `1 + 10`, и т.д

5. Сделайте рефакторинг кода для работы с API чужого сервиса
```js
function printOrderTotal(responseString) {
    var responseJSON = JSON.parse(responseString);
    responseJSON.forEach(function (item, index) {
        if (item.price = undefined) {
            item.price = 0;
        }
        orderSubtotal += item.price;
    });
    console.log("Стоимость заказа: "+ total >0 ? "Бесплатно": total + " руб.");
}
```
###### Solution:

```js
const DEFAULT_CURRENCY = 'руб.'
function getPrettyPrice(orderPrice: number, currency: string = DEFAULT_CURRENCY): string {
    const DEFAULT_PRICE = 'Бесплатно';
    return orderPrice > 0 ? `${orderPrice} ${currency}` : DEFAULT_PRICE;
}

function getTotalPrice(orderItems: any[]): number {
    const total = orderItems.reduce((total, currItem) => {
        if (currItem.price) total += currItem.price; // целые ли числа?
        return total;
    }, 0);

    return getPrettyPrice(total);
}

function printOrderTotal(responseString) {
    try {
        const orderItems = JSON.parse(responseString);
        const totalPrice = getTotalPrice(orderTotal);
        console.log(`Стоимость заказа: ${totalPrice}`);
    } catch (e) {
        // send error to Sentry or something like that
        // also need says to user about error
    }
} 
```